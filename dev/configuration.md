[Réponses aux questions](./README.md)

# Configuration / Installation

[Installation de l'API](ct13_api/README.md)

[Installation de l'APP](ct13_app/README.md)

## Docker (optionnel)

Voici les éléments docker que nous avons utilisez pour travailler sur le projet (on a utiliser le docker fournis en cours, on a juste modifier l'url de l'API car le port 6000 ne voulais pas être bind. D'autre groupe ont également eu ce problème).

Il est bien évidémment possible d'avoir une configuration totalement différente, celui-ci est juste un exemple, l'important est d'avoir deux containers docker qui tournent et qui soit accessible l'un par l'autre depuis l'hôte.

### Architecture

- Dockerfile
- docker-compose.yml
- dev
  - README.md
  - ct13_api
  - ct13_app

### Dockerfile

```
FROM php:8.1.1-fpm-alpine
ARG USERNAME
ARG UID
ARG EMAIL
ARG NAME

RUN echo "==============================="
RUN echo "$USERNAME ($UID)"
RUN echo "$NAME ($EMAIL)"
RUN echo "==============================="

# installation bash
RUN apk --no-cache update && apk --no-cache add bash git && apk --no-cache add npm && apk --no-cache add nodejs\
&& git config --global user.email $EMAIL \
&& git config --global user.name  $NAME

# installation de composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
&& php composer-setup.php --install-dir=/usr/local/bin \
&& php -r "unlink('composer-setup.php');"

# installation de symfony
RUN wget https://get.symfony.com/cli/installer -O - | bash \
&& mv /root/.symfony/bin/symfony /usr/local/bin/symfony

# installation de Angular
RUN npm install -g typescript  && npm install -g @angular/cli

# gestion utilisateur
RUN adduser -h /home/$USERNAME -D -s /bin/bash -u $UID $USERNAME
USER $USERNAME

WORKDIR /var/www/html
```

### docker-compose.yml

```
version: "3"
services:
  php-fpm:
    build:
      context: .
      dockerfile: Dockerfile
      args:
        USERNAME: f
        UID: 501
        EMAIL: justin.lecas@univ-orleans.fr
        NAME: "Justin Lecas"
    container_name: framework_web_CT
    expose:
      - 8000
      - 6020
      - 6000
    volumes:
      - ./dev:/var/www/html
    ports:
      - "8000:8000"
      - "8010:8010"
      - "6020:4200"
    environment:
      PHP_IDE_CONFIG: serverName=framework_web_angular
      PS1: "[ $$(whoami) | $$(pwd) ] "
```
