[Configuration / Installation globale](../configuration.md)

# Installation de l'APP

Si vous souhaitez installer l'APP, veuillez suivre ces commandes dans l'ordre :

1. cd ct13_app
1. npm install
1. ng serve --host 0.0.0.0

Ne pas oubliez de modifier l'url de l'API `apiUrl: <url-de-l-api>` dans le fichier [environnement.ts](src/environments/environment.ts)
