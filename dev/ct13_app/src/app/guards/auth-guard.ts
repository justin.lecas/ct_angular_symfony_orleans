import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { Auth } from '../entity/auth';

// https://angular.io/api/router/CanActivate
@Injectable()
export class AuthGuard implements CanActivate {
  auth$!: Observable<Auth>;

  constructor(private store: Store<{ auth: Auth }>, private router: Router) {
    this.auth$ = this.store.select('auth');
  }

  canActivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.auth$.pipe(
      map((auth) => {
        if (auth.isLoggedIn) return true;
        return this.router.parseUrl('/film');
      })
    );
  }
}
