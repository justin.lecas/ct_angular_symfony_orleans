import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Categorie, HydraCategorie } from '../entity/categorie';
import { FilmWebService } from '../service/film-web.service';
import { CategorieWebService } from '../service/categorie-web.service';
import { FormFilm, Version } from '../entity/film';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-ajouter-film',
  providers: [Location],
  templateUrl: './ajouter-film.component.html',
  styleUrls: ['./ajouter-film.component.scss'],
})
export class AjouterFilmComponent implements OnInit {
  // Variable pour du rendu graphique
  isLoading: boolean = false;
  categoriesLoading: boolean = false;
  filmLoading: boolean = false;

  // Variable de travail
  categories: Categorie[] = [];
  film: FormFilm = {
    titre: '',
    duree: '',
    resume: '',
    version: Version.francais,
    categorie: null,
    owner: null,
  };
  id!: number | undefined;

  constructor(
    private filmWebService: FilmWebService,
    private categorieWebService: CategorieWebService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}

  goBack(): void {
    this.location.back();
  }

  ngOnInit(): void {
    // On récupère les catégories
    this.categoriesLoading = true;
    this.categorieWebService.all().subscribe((categories) => {
      this.categories = categories['hydra:member'];
      this.categoriesLoading = false;
      // On initialise
      this.init();
    });
  }

  init(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.filmLoading = true;
      this.filmWebService.getById(Number(id)).subscribe((film) => {
        this.id = film.id;
        this.film = { ...film, duree: film.duree.toString() };
        this.syncroCategorie();
        this.filmLoading = false;
      });
    } else {
      this.reset();
    }
  }

  /**
   * Permet de reset les valeurs par défaut de la catégorie
   */
  reset(): void {
    this.film.titre = '';
    this.film.duree = '';
    this.film.resume = '';
    this.film.version = Version.francais;
    this.film.categorie = null;
  }

  /**
   * Ajoute un film
   */
  private ajout(): void {
    this.isLoading = true;
    this.filmWebService.create(this.film).subscribe((r) => {
      this.isLoading = false;
      if (r)
        // Possiblement un utilisateur ne pourrait ne pas avoir d'action précédente dans history,
        // alors on navigue manuellement vers film. (Par exemple s'il accède directement a la page
        // /ajouterFilm.
        /**
        * Après l'ajout on est redirigé sur la catégorie du film ajouté par défaut
        */
        this.router.navigate(['/film'], {queryParams: {categorie_id: this.film?.categorie?.id}});
    });
  }

  /**
   * Lorsque qu'on récupère un film, pour que la catégorie du film soit automatiquement sélectionner, on doit
   * préciser que l'objet catégorie récupérer est le même que celui de l'objet film, et qu'il n'a pas juste les
   * mêmes valeurs, mais bien la même espace mémoire.
   * categorie == categorie n'est pas pareille que { id:1 } == { id: 1 }.
   * On peut également, au lieu
   * de vérifier deux objets identiques, modifier le ngModel du select pour ne récupérer seulement l'id de la catégorie,
   * et faire pareille dans le value de "option".
   */
  private syncroCategorie(): void {
    // Soit on trouve la catégorie identique, soit on met null
    this.film.categorie =
      this.categories.find((c) => c.id == this.film?.categorie?.id) || null;
  }

  enregistrer(): void {
    if (this.isLoading) return;
    // Vérification des champs
    if (
      !this.film.titre ||
      !this.film.duree ||
      !this.film.resume ||
      !this.film.version ||
      !this.film.categorie
    ) {
      alert('Tous les champs sont requis');
      return;
    }
    // Ajout
    if (this.id === undefined) this.ajout();
    // Modification
    else {
      // Met à jour
      const categorie = this.film.categorie;

      this.isLoading = true;
      this.filmWebService
        .modify(this.id, categorie.id, this.film)
        .subscribe((r) => {
          this.isLoading = false;
          if (r) {
            this._snackBar
              .open("L'élément a été correctement modifié.", 'Voir les films', {
                horizontalPosition: 'center',
                verticalPosition: 'top',
                // On laisse 10 secondes pour lui donner la possibilité de retourner voir les films
                duration: 10000,
                panelClass: 'snackbar',
              })
              .onAction()
              .subscribe(() => this.router.navigate(['/film'], {queryParams: {categorie_id: categorie.id}}));
          }
        });
    }
  }
}
