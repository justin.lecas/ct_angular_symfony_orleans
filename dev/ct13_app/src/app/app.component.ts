import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Auth } from './entity/auth';
import { logout } from './store/auth.actions';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'ct13_app';
  auth$!: Observable<Auth>;

  constructor(private store: Store<{ auth: Auth }>, private router: Router) {
    this.auth$ = this.store.select('auth');
  }

  public logout(): void {
    this.store.dispatch(logout());
    this.router.navigateByUrl("/film")
  }
}
