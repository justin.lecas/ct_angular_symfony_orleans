import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { FilmComponent } from './film/film.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AjouterFilmComponent } from './ajouter-film/ajouter-film.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { StoreModule } from '@ngrx/store';
import { authReducer } from './store/auth.reducers';
import { ReactiveComponentModule } from '@ngrx/component';
import { AuthGuard } from './guards/auth-guard';
import { UnlogGuard } from './guards/unlog-guard';
import { httpInterceptorProviders } from './interceptors/http-auth-interceptors';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    FilmComponent,
    AjouterFilmComponent,
    InscriptionComponent,
    ConnexionComponent,
  ],
  imports: [
    StoreModule.forRoot({ auth: authReducer }),
    ReactiveComponentModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatDividerModule,
    MatCardModule,
    FormsModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatIconModule
  ],

  providers: [AuthGuard, UnlogGuard, StoreModule, httpInterceptorProviders],
  bootstrap: [AppComponent],
})
export class AppModule {}
