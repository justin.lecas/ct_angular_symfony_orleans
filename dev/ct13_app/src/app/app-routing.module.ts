import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { FilmComponent } from './film/film.component';
import { AjouterFilmComponent } from './ajouter-film/ajouter-film.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AuthGuard } from './guards/auth-guard';
import { UnlogGuard } from './guards/unlog-guard';

const routes: Routes = [
  {
    path: '',
    component: AccueilComponent,
  },
  {
    path: 'film',
    component: FilmComponent,
  },
  {
    path: 'ajouterFilm',
    component: AjouterFilmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'ajouterFilm/:id',
    component: AjouterFilmComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'connexion',
    component: ConnexionComponent,
    canActivate: [UnlogGuard],
  },
  {
    path: 'inscription',
    component: InscriptionComponent,
    canActivate: [UnlogGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
