// https://angular.io/guide/http#intercepting-requests-and-responses

import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HTTP_INTERCEPTORS,
  HttpErrorResponse,
} from '@angular/common/http';

import { catchError, Observable, throwError } from 'rxjs';
import { Auth } from '../entity/auth';
import { Store } from '@ngrx/store';
import { logout } from '../store/auth.actions';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {
  constructor(private store: Store) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const storage = localStorage.getItem('ct13_auth');
    if (storage) {
      const auth: Auth = JSON.parse(storage);

      // Clone the request and replace the original headers with
      // cloned headers, updated with the authorization.
      const authReq = req.clone({
        headers: req.headers.set('Authorization', auth.token),
      });

      return next.handle(authReq).pipe(
        catchError((e: HttpErrorResponse) => {
          // Déconnecte l'utilisateur automatiquement si le token ne passe pas.
          if (e.status == 401) this.store.dispatch(logout());
          return throwError(() => new Error('Vous avez été déconnecté.'));
        })
      );
    }

    return next.handle(req);
  }
}

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: HttpAuthInterceptor, multi: true },
];
