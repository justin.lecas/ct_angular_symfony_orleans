// https://ngrx.io/guide/store
import { createReducer, on } from '@ngrx/store';
import { Auth } from '../entity/auth';
import { login, logout } from './auth.actions';

const storage = localStorage.getItem('ct13_auth');
let previousState: Auth = { isLoggedIn: false, user: null, token: '' };
if (storage) {
  previousState = JSON.parse(storage);
}

export const initialState: Auth = previousState;

export const authReducer = createReducer(
  initialState,
  on(login, (state, action) => {
    const newState: Auth = {
      isLoggedIn: true,
      user: action.user,
      token: action.user.token,
    };

    localStorage.setItem('ct13_auth', JSON.stringify(newState));

    // Si jamais une erreur survient
    return newState || state;
  }),
  on(logout, () => {
    localStorage.removeItem('ct13_auth');
    return { isLoggedIn: false, user: null, token: '' };
  })
);
