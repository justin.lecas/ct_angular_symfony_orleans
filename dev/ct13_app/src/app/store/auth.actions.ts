import { createAction, props } from '@ngrx/store';
import { User } from '../entity/user';

export const login = createAction('[Login] PutUser', props<{ user: User }>());
export const logout = createAction('[Logout] RemoveUser');
