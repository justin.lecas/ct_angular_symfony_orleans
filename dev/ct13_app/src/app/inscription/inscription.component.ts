import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, throwError } from 'rxjs';
import { HydraError, HydraViolation, UserForm } from '../entity/user';
import { AuthWebService } from '../service/auth-web.service';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss'],
})
export class InscriptionComponent implements OnInit {
  form: UserForm = { username: '', password: '' };
  errors?: HydraViolation[];
  isLoading: boolean = false;

  constructor(private authWebService: AuthWebService, private router: Router,
              private _snackBar: MatSnackBar) {}

  ngOnInit(): void {}

  // On handle les erreurs

  gestionError(error: HttpErrorResponse) {
    this.isLoading = false
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);

      console.log(
        "Il semblerait que l'API ne soit pas atteignable. Vérifiez votre configuration. N'oubliez pas de modifier votre fichier environnement.ts. N'oubliez pas de démarrer votre serveur Symfony."
      );
    } else {
      const hydraError: HydraError = error.error;
      this.errors = hydraError.violations;
      return throwError(() => new Error(hydraError['hydra:description']));
    }
    // Return an observable with a user-facing error message.
    return throwError(
      () => new Error('Something bad happened; please try again later.')
    );
  }

  notification(user : string){
    this._snackBar.open("Félicitations!! Vous êtes bien inscrit "+user, "", {
      horizontalPosition: "center",
      verticalPosition: "top",
      panelClass: 'snackbar',
      duration: 300 });
  }

  submit(): void {
    if(this.isLoading)
      return ;

    this.isLoading = true;
    this.errors = undefined;
    this.authWebService
      .inscription(this.form)
      // Fonction flecher, pour le binding, pour pouvoir utiliser this dans la fonction gestionError
      .pipe(catchError((e: HttpErrorResponse) => this.gestionError(e)))
      .subscribe((user) => {
        this.notification(user.username);
        this.router.navigateByUrl('/connexion');
      });
  }
}
