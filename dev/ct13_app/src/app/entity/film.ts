import { Categorie } from './categorie';
import { User } from './user';

// Le formulaire prend tout en string, pour une meilleur gestion graphique avec les inputs, donc
// on ne fait pas d'héritage ici
export enum Version {
  original = 'VORIGINALE',
  francais = 'VFRANCAISE',
}

export interface FormFilm {
  titre: string;
  duree: string;
  resume: string;
  version: Version;
  categorie: Categorie | null;
  owner: User | null;
}

export interface Film {
  id: number;
  titre: string;
  duree: number;
  resume: string;
  version: Version;
  categorie: Categorie;
  owner: User;
}

// api response interface for Symfony api platform Hydra
export interface HydraFilm {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Film[];
  'hydra:totalItems': string;
  'hydra:view': {
    '@id': 'string';
    '@type': 'string';
    'hydra:first': 'string';
    'hydra:last': 'string';
    'hydra:previous': 'string';
    'hydra:next': 'string';
  };
}
