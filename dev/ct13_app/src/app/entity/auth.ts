import { User } from './user';

export interface Auth {
  isLoggedIn: boolean;
  user: User | null;
  token: string;
}
