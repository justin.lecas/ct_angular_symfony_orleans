export interface User {
  id: number;
  username: string;
  roles: string[];
  token: string;
}

export interface UserForm {
  username: string;
  password: string;
}

export interface HydraViolation {
  propertyPath: string;
  message: string;
  code: string;
}

export interface LoginError {
  message: string;
}

export interface HydraError {
  '@context': string;
  '@type': string;
  'hydra:description': string;
  'hydra:title': string;
  violations: HydraViolation[];
}
