export interface Categorie {
  id: number;
  nom: string;
}

// api response interface for Symfony api platform Hydra
export interface HydraCategorie {
  '@context': string;
  '@id': string;
  '@type': string;
  'hydra:member': Categorie[];
  'hydra:totalItems': string;
  'hydra:view': {
    '@id': 'string';
    '@type': 'string';
    'hydra:first': 'string';
    'hydra:last': 'string';
    'hydra:previous': 'string';
    'hydra:next': 'string';
  };
}