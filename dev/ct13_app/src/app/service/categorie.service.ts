import { Injectable } from '@angular/core';
import { Categorie } from '../entity/categorie';

@Injectable({
  providedIn: 'root',
})
export class CategorieService {
  categories: Categorie[] = [
    { id: 1, nom: 'Comédie' },
    { id: 2, nom: 'Aventure' },
    { id: 3, nom: 'Action' },
    { id: 4, nom: 'Drame' },
    { id: 5, nom: 'Horreur' },
    { id: 6, nom: 'Romance' },
  ];

  constructor() {}

  get(): Categorie[] {
    return this.categories;
  }

  getById(id: number): Categorie | null {
    const categorie: Categorie | undefined = this.categories.find(
      (c) => c.id == id
    );
    return categorie || null;
  }

  getByNom(nom: string): Categorie | undefined {
    return this.categories.find((c) => c.nom == nom);
  }

  add(nom: string): void {
    const id = Math.max(...this.categories.map((cat) => cat.id)) + 1;
    this.categories.push({ id, nom });
  }

  update(id: number, nom: string): boolean {
    let categorie = this.getById(id);
    if (categorie) {
      categorie.nom = nom;
      return true;
    }
    return false;
  }

  remove(id: number): boolean {
    let categorie = this.getById(id);
    if (categorie) {
      const index: number = this.categories.indexOf(categorie);
      if (index != -1) {
        this.categories.splice(index, 1);
        return true;
      }
    }
    return false;
  }
}
