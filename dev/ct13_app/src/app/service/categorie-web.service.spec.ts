import { TestBed } from '@angular/core/testing';

import { CategorieWebService } from './categorie-web.service';

describe('CategorieWebService', () => {
  let service: CategorieWebService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CategorieWebService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});