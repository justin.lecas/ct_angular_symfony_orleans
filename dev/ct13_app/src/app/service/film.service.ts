import { Injectable } from '@angular/core';
import { Categorie } from '../entity/categorie';
import { Film } from '../entity/film';
import { CategorieService } from './categorie.service';

@Injectable({
  providedIn: 'root',
})
export class FilmService {
  films: Film[] = [];

  constructor(private categorieService: CategorieService) {
    const categories: Categorie[] = this.categorieService.get();
    for (let i = 0; i < 12; i++) {
      const categorie =
        categories[Math.floor(Math.random() * categories.length)];
      const nbVersion = Math.floor(Math.random() * 100);
      const version = nbVersion % 2 ? 'VFRANCAISE' : 'VORIGINALE';
      this.add('titre ' + i, 100 + i, 'resume ' + i, version, categorie);
    }
  }

  get(): Film[] {
    return this.films;
  }

  getById(id: number): Film | null {
    const film: Film | undefined = this.films.find((c) => c.id == id);
    return film || null;
  }

  add(
    titre: string,
    duree: number,
    resume: string,
    version: 'VFRANCAISE' | 'VORIGINALE',
    categorie: Categorie) : void {
    let newid: number = 1;
    if (this.films.length > 0)
      newid = Math.max(...this.films.map((cat) => cat.id)) + 1;
    this.films.push({ id:newid, titre, duree, resume, version, categorie });
  }

  update(id: number, film: Film): boolean {
    let oldfilm = this.getById(id);
    if (oldfilm) {
      oldfilm.titre = film.titre
      oldfilm.duree = film.duree
      oldfilm.resume = film.resume
      oldfilm.categorie = film.categorie
      oldfilm.version = film.version
      return true;
    }
    return false;
  }


  remove(film : Film): void {
    const index: number = this.films.indexOf(film);
    this.films.splice(index, 1);
  }
}
