import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Film, FormFilm, HydraFilm } from '../entity/film';

@Injectable({
  providedIn: 'root',
})
export class FilmWebService {
  // Gestion des erreurs, bonne pratiques, pour soit afficher un message au client, ou gérer un retour dans le cadre d'une erreur
  // https://angular.io/guide/http#error-handling
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);

      console.log(
        "Il semblerait que l'API ne soit pas atteignable. Vérifiez votre configuration. N'oubliez pas de modifier votre fichier environnement.ts. N'oubliez pas de démarrer votre serveur Symfony."
      );
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    // Return an observable with a user-facing error message.
    return throwError(
      () => new Error('Something bad happened; please try again later.')
    );
  }

  private apiUrl = `${environment.apiUrl}/films`; // URL to web api
  /**
   * URL IRI pour mettre la catégorie, Hydra ne tolère pas le nested document
   * categorie: { id: 1, nom: "Drame" } ne fonctionnera pas dans le film car il indique qu'il doit créer une catégorie 1 au lieu de dire que le film doit avoir comme catégorie la catégorie 1
   * pour se faire, il faut préciser un IRI, ce iri décrit l`URL de l'objet a être imbriquer et s'écrit sous la forme /api/categories/:id
   */
  private categorieIri = '/api/categories/';

  constructor(private http: HttpClient) {}

  public all(params?: HttpParams): Observable<HydraFilm> {
    return this.http
      .get<HydraFilm>(this.apiUrl, { params })
      .pipe(catchError(this.handleError));
  }

  public getById(id: number): Observable<Film> {
    return this.http
      .get<Film>(this.apiUrl + '/' + id.toString(), {
        observe: 'body',
        responseType: 'json',
      })
      .pipe(catchError(this.handleError));
  }

  public remove(id: number): Observable<boolean> {
    return this.http
      .delete(this.apiUrl + '/' + id.toString(), {
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        catchError(this.handleError),
        map((response) => response.status === 204)
      );
  }

  public modify(
    id: number,
    categorie_id: number,
    film: FormFilm
  ): Observable<boolean> {
    const { owner, ...data } = {
      ...film,
      categorie: this.categorieIri + categorie_id,
    };
    return this.http
      .put(
        this.apiUrl + '/' + id.toString(),
        {
          ...data,
          duree: Number(data.duree),
        },
        {
          observe: 'response',
          responseType: 'json',
        }
      )
      .pipe(
        catchError(this.handleError),
        map((response) => response.status === 200)
      );
  }

  public create(film: FormFilm): Observable<boolean> {
    const { owner, ...data } = {
      ...film,
      categorie: this.categorieIri + film?.categorie?.id,
    };
    return this.http
      .post(this.apiUrl, data, {
        observe: 'response',
        responseType: 'json',
      })
      .pipe(
        catchError(this.handleError),
        map((response) => response.status === 201)
      );
  }
}
