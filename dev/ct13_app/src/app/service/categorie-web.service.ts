import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Categorie, HydraCategorie } from '../entity/categorie';

@Injectable({
  providedIn: 'root',
})
export class CategorieWebService {
  // Gestion des erreurs, bonne pratiques, pour soit afficher un message au client, ou gérer un retour dans le cadre d'une erreur
  // https://angular.io/guide/http#error-handling
  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);

      // Dans le cadre de cette application, si le status est 0, c'est que le site cible ne renvoie aucune réponse, probablement un oublie du développeur, on le précise.
      console.log(
        "Il semblerait que l'API ne soit pas atteignable. Vérifiez votre configuration. N'oubliez pas de modifier votre fichier environnement.ts. N'oubliez pas de démarrer votre serveur Symfony."
      );
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `,
        error.error
      );
    }
    // Return an observable with a user-facing error message.
    return throwError(
      () => new Error('Something bad happened; please try again later.')
    );
  }

  private apiUrl = `${environment.apiUrl}/categories`; // URL to web api

  constructor(private http: HttpClient) {}

  public all(): Observable<HydraCategorie> {
    return this.http
      .get<HydraCategorie>(this.apiUrl)
      .pipe(catchError(this.handleError));
  }

  public getById(id: number): Observable<Categorie> {
    return this.http
      .get<Categorie>(this.apiUrl + '/' + id.toString(), {
        observe: 'body',
        responseType: 'json',
      })
      .pipe(catchError(this.handleError));
  }
}
