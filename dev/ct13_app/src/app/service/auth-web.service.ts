import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User, UserForm } from '../entity/user';

@Injectable({
  providedIn: 'root',
})
export class AuthWebService {
  private apiInscription = `${environment.apiUrl}/users`; // URL to web api
  private apiConnexion = `${environment.apiUrl}/auth/login`;

  constructor(private http: HttpClient) {}

  public inscription(userForm: UserForm): Observable<User> {
    return this.http.post<User>(this.apiInscription, userForm);
  }

  public connexion(userForm: UserForm): Observable<User> {
    return this.http.post<User>(this.apiConnexion, userForm);
  }
}
