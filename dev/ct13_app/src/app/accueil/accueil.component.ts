import { Component, OnInit } from '@angular/core';

interface Etudiant {
  numEtu: string;
  nom: string;
  prenom: string;
}
@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  displayedColumns: string[] = ['numEtu', 'nom', 'prenom']
  listeEtu: Etudiant[] = [
    {numEtu: "2199426", nom: "BARRY", prenom: "Fatoumata"},
    {numEtu: "22010406", nom: "DIALLO", prenom: "Amadou"},
    {numEtu: "22108939", nom: "DIALLO", prenom: "Fatoumata"},
    {numEtu: "2197502", nom: "LECAS", prenom: "Justin"},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
