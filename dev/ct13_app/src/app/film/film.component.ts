import { Component, OnInit, ViewChild } from '@angular/core';
import { Film } from '../entity/film';
import { MatTable } from '@angular/material/table';
import { FilmWebService } from '../service/film-web.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {
  map,
  merge,
  Observable,
  startWith,
  Subscription,
  switchMap,
} from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { CategorieWebService } from '../service/categorie-web.service';
import { Categorie, HydraCategorie } from '../entity/categorie';
import { MatSelect } from '@angular/material/select';
import { Store } from '@ngrx/store';
import { Auth } from '../entity/auth';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss'],
})
export class FilmComponent implements OnInit {
  filmsSubscription?: Subscription;
  categories: Categorie[] = [];
  categorie!: Categorie;
  listefilm: Array<Film> = [];
  displayedColumns: string[] = [
    'id',
    'titre',
    'duree',
    'resume',
    'version',
    'actions',
  ];

  isLoading!: boolean;
  resultsLength = 0;
  // Initialisation variable de résultat
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<any>;
  @ViewChild('categoriesSelect') categoriesSelect!: MatSelect;

  auth$!: Observable<Auth>;
  constructor(
    private filmWebService: FilmWebService,
    private categorieWebService: CategorieWebService,
    private route: ActivatedRoute,
    private store: Store<{ auth: Auth }>
  ) {
    this.auth$ = this.store.select('auth');
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.categorieWebService.all().subscribe((c) => this.initCategories(c));
  }

  /**
   * Initialise les catégories, et la catégorie par défaut
   * @param categories 
   * @returns 
   */
  initCategories(categories: HydraCategorie): void {
    this.categories = categories['hydra:member'];
    if (this.categories.length > 0) {
      const categorie_id: string | null = this.route.snapshot.queryParams['categorie_id']
      // Si l'ID d'une categorie est préciser, alors on charge celle la par défaut
      if(categorie_id) {
        const c: Categorie | undefined = this.categories.find(cat => cat.id == Number(categorie_id))
        // Si la categorie n'est pas trouvé, on prend par défaut la première catégorie.
        if(c) {
          this.categorie = c;
          return ;
        }
      }
      // On met le premier par défaut
      this.categorie = this.categories[0];
    }
  }

  ngAfterViewInit(): void {
    // Enlever le warning NG0100
    setTimeout(
      () =>
        /**
         * L'avantage de passer par cette méthode au lieu de passer par un ngModelChange sur le bouton select,
         * c'est qu'automatiquement, en fonction du changement de la catégorie, les éléments vont se charger,
         * ça veut dire que même si on modifie la catégorie via notre code, cela sera détecter ici.
         * Pas besoin de repréciser qu'il faut recharger les catégories sur la page.
         */
        this.categoriesSelect.optionSelectionChanges.subscribe(() =>
          this.load()
        ),
      0
    );
  }

  load() {
    if (this.filmsSubscription) this.filmsSubscription.unsubscribe();

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    // Lors du clique sur le bouton, et seulement a ce moment, on reinit le paginator a la page 0
    this.paginator.firstPage();

    // On applique un écouteur sur le trie et sur la pagination
    this.filmsSubscription = merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        // Récupération des données
        switchMap(() => {
          // On charge les données
          this.isLoading = true;

          // Création de la requête GET
          let params = new HttpParams({
            fromObject: {
              page: this.paginator.pageIndex + 1,
              itemsPerPage: this.paginator.pageSize
            },
          });
          params = params.append(
            'order[' + this.sort.active + ']',
            this.sort.direction
          );
          // On passe la categorie avec API Platform
          if (this.categorie)
            params = params.append('categorie.id', this.categorie.id);

          return this.filmWebService.all(params);
        }),
        map((data) => {
          // On arrête de charger les données
          this.isLoading = false;

          this.resultsLength = Number(data['hydra:totalItems']);
          return data['hydra:member'];
        })
      )
      .subscribe((data) => {
        this.listefilm = data;
      });
  }

  remove(id: number) {
    const confirm = window.confirm(
      'Cette action est irréversible, voulez-vous continuez ?'
    );
    if (confirm) {
      this.isLoading = true;
      this.filmWebService.remove(id).subscribe((r) => {
        this.load();
      });
    }
  }
}
