import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { catchError, Observable, throwError } from 'rxjs';
import { Auth } from '../entity/auth';
import { LoginError, UserForm } from '../entity/user';
import { AuthWebService } from '../service/auth-web.service';
import { login } from '../store/auth.actions';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss'],
})
export class ConnexionComponent implements OnInit {
  form: UserForm = { username: '', password: '' };
  error?: LoginError;
  auth$!: Observable<Auth>;
  isLoading: boolean = false;

  constructor(
    private authWebService: AuthWebService,
    private store: Store<{ auth: Auth }>,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    this.auth$ = this.store.select('auth');
  }

  ngOnInit(): void {}

  // On handle les erreurs
  gestionError(error: HttpErrorResponse) {
    this.isLoading = false
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);

      console.log(
        "Il semblerait que l'API ne soit pas atteignable. Vérifiez votre configuration. N'oubliez pas de modifier votre fichier environnement.ts. N'oubliez pas de démarrer votre serveur Symfony."
      );
    } else {
      this.error = error.error;
      return throwError(() => new Error(this?.error?.message || error.message));
    }
    // Return an observable with a user-facing error message.
    return throwError(
      () => new Error('Something bad happened; please try again later.')
    );
  }

  alert(user : string){
    this._snackBar.open("Bienvenue ou Bon retour parmi nous " + user, "", {
      horizontalPosition: "center",
      verticalPosition: "top",
      panelClass: 'snackbar',
      duration: 2500 });
  }

  submit(): void {
    if(this.isLoading)
      return ;

    this.isLoading = true;
    this.error =  undefined;
    this.authWebService
      .connexion(this.form)
      // Fonction flecher, pour le binding, pour pouvoir utiliser this dans la fonction gestionError
      .pipe(catchError((e: HttpErrorResponse) => this.gestionError(e)))
      .subscribe((user) => {
        this.store.dispatch(login({ user }));
        this.alert(user.username);
        this.router.navigateByUrl('/film');
      });

  }
}
