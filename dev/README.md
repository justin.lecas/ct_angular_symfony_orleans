[Commandes d'installation du projet (optionnel)](./configuration.md)

# Membre du groupe

Groupe 13 :

- 22108939 DIALLO Fatoumata
- 2197502 LECAS Justin
- 2199426 BARRY Fatoumata
- 22010406 DIALLO Amadou

# Question 1

- ng new ct13_app
- ng serve --host 0.0.0.0

Pour démarrer le server angular, on peut également rajouter l'attribut --poll pour les utilisateurs de windows, dans le cas d'erreur avec le hot reload.

# Question 2

- ng g c accueil
- ng g c film

# Question 3

- ng add @angular/material

# Question 4

- ng g s service/categorie

# Question 5

- ng g s service/film

# Question 6

- ng g c ajouterFilm

# Question 7

Lors de la modification, on ne redirige pas automatiquement sur les films, on demande confirmation via un bouton alert, car lors d'une modification, on peut vouloir rester sur l'objet et continuer a travailler dessus.

# Question 8

- symfony new ct13_api --full
- symfony console doctrine:database:create
- symfony console make:entity Categorie
- symfony console make:entity Film
- symfony console make:migration
- symfony console doctrine:migrations:migrate
- symfony composer req --dev orm-fixtures
- symfony composer req fakerphp/faker
- symfony console doctrine:fixtures:load
- symfony composer req api

Ajout de la possibilité de trier les films, et de récupérer les films par l'id de la catégorie => ApiFilter.

# Question 9

- ng g s service/filmWeb
- ng g s service/categorieWeb

# Question 10

Grâce à API Platform on a donné la possibilité de pouvoir ordonner par ID, titre, durée et version. On a également ajouté la pagination. Ce qui signifie qu'après ajout d'un film, vous devrez vous déplacez à la dernière page, ou ordonnez par ID descendant pour bien voir l'ajout de votre film.

# Question 11

On a choisi de connecter notre composant select et d'écouter ses changements directement dans notre component, grâce à son subscriber : this.categoriesSelect.optionSelectionChanges.

L'avantage de passer par cette méthode au lieu de passer par un ngModelChange sur le bouton select, est : qu'automatiquement, en fonction du changement de la catégorie, les éléments vont se charger. La modification d'une catégorie via notre code, sera détecté automatiquement.

Pas besoin de repréciser qu'il faut recharger les catégories sur la page.

# Question 12

## API

Création de la sécurité utilisateur

- symfony console make:user

Ajout du token : on va gérer une authentification utilisateur par un token unique. L'utilisateur se connectera, ensuite ce token lui sera fourni, et pour vérifier son authentification, le client devra envoyer ce token en paramêtre du header.

- symfony console make:entity User
- symfony console make:migration
- symfony console doctrine:migrations:migrate

Ajout de l'authentification : si le paramêtre Authorization est renseigné (https://symfony.com/doc/current/security/custom_authenticator.html), alors on connecte l'utilisateur.

- symfony console make:auth

Gestion de la connexion : app_login, si username et password sont bien les informations de l'utilisateur, on lui renvoie ses données.

- symfony console make:controller Auth

Utilisation de l'inscription avec API Platform, encoder le mot de passe nécessite un dataProvider :
https://api-platform.com/docs/core/data-persisters/#creating-a-custom-data-persister

## APP

Ajout des deux pages : connexion et inscription.

- ng g c connexion
- ng g c inscription

Ajout d'un service auth que l'on nommera authWeb, pour rester dans la logique syntaxique des services que l'on a actuellement.

- ng g s service/authWeb

Ajout de NgRx : NgRx est le store d'angular, ce qui nous permettra de partager l'objet utilisateur connecté à tous les composants qui s'y abonneront.
https://ngrx.io/docs

- npm install @ngrx/store --save

Ajout du module d'utilisation de template, pour pouvoir gérer les observables côté template.
https://ngrx.io/guide/component/let

- npm install @ngrx/component --save

# Question 13

On ajoute l'attribut owner à film.

- symfony console make:entity Film
- symfony console make:migration

On vide la base, car les films doivent maintenant obligatoirement avoir un owner.

- symfony console doctrine:database:drop --force
- symfony console doctrine:database:create
- symfony console doctrine:migrations:migrate

On modifie les fixtures, on créer un utilisateur par défaut (userDefaut, motdepasseDefaut) et on regénère les datas :

- symfony console doctrine:fixtures:load
