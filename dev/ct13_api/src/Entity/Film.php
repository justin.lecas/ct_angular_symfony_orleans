<?php

namespace App\Entity;

use App\Repository\FilmRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

#[ApiResource(
    normalizationContext: [ 'groups' => ['read:collection']],
    collectionOperations: [
        "get",
        "post" => ["security" => "is_granted('ROLE_USER')"],
    ],
    itemOperations: [
        "get",
        "put" => ["security" => "is_granted('ROLE_USER') and object.getOwner() == user"],
        "delete" => ["security" => "is_granted('ROLE_USER') and object.getOwner() == user"],
    ],
)]
#[ApiFilter(OrderFilter::class, properties: ['id', 'titre', 'duree', 'version'], arguments: ['orderParameterName' => 'order'])]
#[ORM\Entity(repositoryClass: FilmRepository::class)]
class Film
{
    const VERSIONS = ['VORIGINALE', 'VFRANCAISE'];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[ApiProperty(identifier: true)]
    #[Groups(['read:collection'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:collection'])]
    private $titre;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read:collection'])]
    private $duree;

    #[ORM\Column(type: 'text')]
    #[Groups(['read:collection'])]
    private $resume;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read:collection'])]
    #[Assert\Choice(choices:Film::VERSIONS, message:"Veuillez choisir l'un des choix correspondants : VORIGINALE ou VFRANCAISE")]
    private $version;

    #[ORM\ManyToOne(targetEntity: Categorie::class, inversedBy: 'films')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:collection'])]
    #[ApiFilter(SearchFilter::class, properties: ['categorie.nom' => 'ipartial'])]
    #[ApiFilter(SearchFilter::class, properties: ['categorie.id' => 'exact'])]
    private $categorie;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'films')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:collection'])]    
    private $owner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
