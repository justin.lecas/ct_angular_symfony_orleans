<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[Route(path: '/api/auth', name: 'api_auth')]
class AuthController extends AbstractController
{
    #[Route(path: '/login', name: 'login', methods: ["POST"])]
    public function login(Request $request, UserRepository $userRepository, UserPasswordHasherInterface $passwordHasher): Response
    {
        if($this->getUser())
            return $this->json($this->getUser(), Response::HTTP_ACCEPTED);

        $json_request = json_decode(
            $request->getContent(),
            true
        );

        if(!key_exists("username", $json_request) || empty($json_request["username"]))
            return $this->json(["message" => "Le username doit être renseigner"], Response::HTTP_FORBIDDEN);

        if(!key_exists("password", $json_request) || empty($json_request["password"]))
            return $this->json(["message" => "Le mot de passe doit être renseigner"], Response::HTTP_FORBIDDEN);

        $username = $json_request["username"];
        $password = $json_request["password"];

        // Get user by username
        $user = $userRepository->findOneBy(["username" => $username]);

        // Check password by username
        if (!$user || !$passwordHasher->isPasswordValid($user, $password)) {
            // https://symfony.com/doc/current/security/passwords.html
            return $this->json(["message" => "Vos informations ne sont pas valides"], Response::HTTP_FORBIDDEN);
        }

        // Send user
        return $this->json($user, Response::HTTP_ACCEPTED);
    }
}
