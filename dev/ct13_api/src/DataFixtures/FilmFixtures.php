<?php

namespace App\DataFixtures;

use App\Entity\Film;
use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\UserFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class FilmFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create();

        $categories = [];

        $categorieComedie = new Categorie();
        $categorieComedie->setNom("Comédie");
        $manager->persist($categorieComedie);
        array_push($categories, $categorieComedie);

        $categorieAventure = new Categorie();
        $categorieAventure->setNom("Aventure");
        $manager->persist($categorieAventure);
        array_push($categories, $categorieAventure);

        $categorieAction = new Categorie();
        $categorieAction->setNom("Action");
        $manager->persist($categorieAction);
        array_push($categories, $categorieAction);

        $categorieDrame = new Categorie();
        $categorieDrame->setNom("Drame");
        $manager->persist($categorieDrame);
        array_push($categories, $categorieDrame);

        $categorieHorreur = new Categorie();
        $categorieHorreur->setNom("Horreur");
        $manager->persist($categorieHorreur);
        array_push($categories, $categorieHorreur);

        $categorieRomance = new Categorie();
        $categorieRomance->setNom("Romance");
        $manager->persist($categorieRomance);
        array_push($categories, $categorieRomance);

        $manager->flush();

        $user = $this->getReference(UserFixtures::DEFAUT_USER_REFERENCE);

        for($i = 0; $i < 100; $i++) {
            $film = new Film();
            
            $version = $faker->boolean() ? "VORIGINALE" : "VFRANCAISE";
            $film->setTitre($faker->name())
                ->setDuree($faker->numberBetween(100, 140))
                ->setResume($faker->text())
                ->setVersion($version)
                ->setCategorie($categories[rand(0, count($categories)-1)])
                ->setOwner($user);
            
            $manager->persist($film);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
