<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public const DEFAUT_USER_REFERENCE = 'defaut-user';

    public function __construct(UserPasswordHasherInterface $passwordHasher){
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setUsername('userDefaut')
            ->setPassword($this->passwordHasher->hashPassword($user, 'motdepasseDefaut'));

        $manager->persist($user);
        $manager->flush();

        $this->addReference(self::DEFAUT_USER_REFERENCE, $user);
    }
}