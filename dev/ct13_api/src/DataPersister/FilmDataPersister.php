<?php
namespace App\DataPersister;

// https://symfony.com/doc/current/security.html#registering-the-user-hashing-passwords
// https://api-platform.com/docs/core/data-persisters/#creating-a-custom-data-persister
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Film;
use Symfony\Component\Security\Core\Security;

final class FilmDataPersister implements ContextAwareDataPersisterInterface
{
    private $security;
    private $decorated;

    public function __construct(ContextAwareDataPersisterInterface $decorated, Security $security)
    {
        $this->decorated = $decorated;
        $this->security = $security;
    }

    public function supports($data, array $context = []): bool
    {
        return $this->decorated->supports($data, $context) && $data instanceof Film;
    }

    public function persist($data, array $context = [])
    {
        if (
            ($context['collection_operation_name'] ?? null) === 'post' ||
            ($context['graphql_operation_name'] ?? null) === 'create' || 
            ($context['collection_operation_name'] ?? null) === 'put' ||
            ($context['collection_operation_name'] ?? null) === 'update'  
        ) {
            $data->setOwner($this->security->getUser());
        }

        $result = $this->decorated->persist($data, $context);

        return $result;
    }

    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }
}