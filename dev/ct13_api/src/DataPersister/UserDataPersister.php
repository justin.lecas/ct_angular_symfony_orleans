<?php
namespace App\DataPersister;

// https://symfony.com/doc/current/security.html#registering-the-user-hashing-passwords
// https://api-platform.com/docs/core/data-persisters/#creating-a-custom-data-persister
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private $decorated;
    private $passwordHasher;

    public function __construct(ContextAwareDataPersisterInterface $decorated, UserPasswordHasherInterface $passwordHasher)
    {
        $this->decorated = $decorated;
        $this->passwordHasher = $passwordHasher;
    }

    public function supports($data, array $context = []): bool
    {
        return $this->decorated->supports($data, $context) && $data instanceof User;
    }

    public function persist($data, array $context = [])
    {
        if (
            
                ($context['collection_operation_name'] ?? null) === 'post' ||
                ($context['graphql_operation_name'] ?? null) === 'create'
            
        ) {
            if ($data->getPassword()) {
                $hashedPassword = $this->passwordHasher->hashPassword(
                    $data,
                    $data->getPassword()
                );
                $data->setPassword($hashedPassword);
            }
        }

        $result = $this->decorated->persist($data, $context);

        return $result;
    }

    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }
}