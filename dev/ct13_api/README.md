[Configuration / Installation globale](../configuration.md)

# Installation de l'API

Si vous souhaitez installer l'API, veuillez suivre ces commandes dans l'ordre :

1. cd ct13_api
1. symfony composer install
1. symfony console doctrine:database:create
1. symfony console doctrine:migrations:migrate
1. symfony console doctrine:fixtures:load
1. symfony server:start --no-tls --d
